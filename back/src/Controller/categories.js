import Express from 'express'

const app = Express();

export default async (controller, isLoggedIn) => {
    app.get('/', (req, res, next) => res.send("Ok from Categories datas"))

    // CREATE
    app.post("/new", isLoggedIn, async (req, res, next) => {
        const user_id = req.user.sub
        try {
            const {
                id,
                name,
                color
            } = req.query;
            const result = await controller.ins_UserCategory({
                id, name, color, user_id
            });
            res.json({
                success: true,
                result
            });
        } catch (e) {
            next(e);
        }
    });

    // DELETE
    app.get("/delete/:id", isLoggedIn, async (req, res, next) => {
        const user_id = req.user.sub
        try {
            const {
                id
            } = req.params;
            const result = await controller.del_UserCategory({
                id,
                user_id
            });
            res.json({
                success: true,
                result
            })
        } catch (e) {
            next(e)
        }
    })

    /**
     * Params : {id} | Int field
     * Props : {name} | Text Field | User Textbox Input
     * Props : {color} | Text Field | User Color Picker Input
     * Use to update an existing Category
     */
    app.post("/update/:id", isLoggedIn, async (req, res, next) => {
        const user_id = req.user.sub
        try {
            const {
                id
            } = req.params;
            const {
                name,
                color
            } = req.query;
            const result = await controller.upd_UserCategory(id, {
                name,
                color,
                user_id
            });
            res.json({
                success: true,
                result
            });
        } catch (e) {
            next(e);
        }
    });

    app.get('/get/:id', async (req, res, next) => {
        try {
            const {
                id
            } = req.params
            const result = await controller.sel_UserCategory(id)
            res.json({
                success: true,
                result: result
            })
        } catch (e) {
            next(e)
        }
    })

    app.get("/list", async (req, res, next) => {
        const user_id = req.user.sub
        try {
            const {
                name
            } = req.query;
            const result = await controller.sel_UserCategories({
                name,
                user_id
            });
            res.json({
                success: true,
                result: result
            });
        } catch (e) {
            next(e);
        }
    });

    return app;
}
