import sqlite from 'sqlite'
import SQL from 'sql-template-strings';

const nowForSQLite = () => new Date().toISOString().replace('T', ' ').replace('Z', '');

const joinSQLStatementKeys = (keys, values, delimiter, keyValueSeparator = '=') => {
  return keys
    .map(propName => {
      const value = values[propName];
      if (value !== null && typeof value !== "undefined") {
        return SQL ``.append(propName).append(keyValueSeparator).append(SQL `${value}`);
      }
      return false;
    })
    .filter(Boolean)
    .reduce((prev, curr) => prev.append(delimiter).append(curr));
};

const initializeDatabase = async () => {
  // Force to use the current dir path
  const path = require('path');
  const dbPath = path.resolve(__dirname, './db.sqlite');
  const db = await sqlite.open(dbPath);

  //#region Categories_Queries
  const ins_UserCategory = async (props) => {
    if (!props ||
      !props.name ||
      !props.color ||
      !props.user_id) {
      throw new Error(`you must provide a name and a color in order to create a new category`);
    }

    const {
      name,
      color,
      user_id
    } = props;
    try {
      const result = await db.run(
        SQL `INSERT INTO user_categories (name, color, user_id) VALUES
      (${name}, ${color}, ${user_id}})`
      );
      const id = result.stmt.lastID;
      return id;
    } catch (e) {
      throw new Error(`couldn't insert this combination: ${e.message}`);
    }
  }

  const del_UserCategory = async (props) => {
    const {
      id,
      user_id
    } = props
    try {
      const result = await db.run(
        SQL `DELETE FROM user_categories WHERE id = ${id} AND user_id = ${user_id}`
      );
      if (result.stmt.changes === 0) {
        throw new Error(`category "${id}" does not exist or wrong user_id`);
      }
      return true
    } catch (e) {
      throw new Error(`couldn't delete the category "${id}": ` + e.message)
    }
  }

  const upd_UserCategory = async (id, props) => {
    if (
      (!props, !props.user_id || !(props.name || props.color))
    ) {
      throw new Error(
        `you must provide a name or a color, with the correct user_id`
      );
    }
    try {
      const previousProps = await sel_UserCategory(id)
      const newProps = { ...previousProps,
        ...props
      }
      const statement = SQL `UPDATE user_categories SET `
        .append(
          joinSQLStatementKeys(
            ["name", "color"],
            newProps,
            ", "
          )
        )
        .append(SQL ` WHERE `)
        .append(
          joinSQLStatementKeys(
            ["id", "user_id"], {
              id: id,
              user_id: props.user_id
            },
            " AND "
          )
        );
      const result = await db.run(statement);
      if (result.stmt.changes === 0) {
        throw new Error(`no changes were made`);
      }
      return true;
    } catch (e) {
      throw new Error(`couldn't update the category ${id}: ` + e.message);
    }
  };

  const sel_UserCategory = async id => {
    try {
      const catResult = await db.all(
        SQL `SELECT * FROM user_categories WHERE id = ${id}`
      );
      if (!catResult) {
        throw new Error(`category id (${id}) not found`)
      }
      return catResult;
    } catch (e) {
      throw new Error(`couldn't get the category id (${id}): ` + e.message)
    }
  }

  const sel_UserCategories = async props => {
    const {
      name,
      user_id
    } = props;
    try {
      const query = SQL `SELECT rowid, id, name, color FROM user_categories WHERE user_id = ${user_id}`;
      if (name)
        query.append(`AND name=${name}`);

      const catList = await db.all(query);
      if (!catList) {
        throw new Error(`User_Categories is empty for user_id (${user_id})`)
      }
      return catList;

    } catch (e) {
      throw new Error(`couldn't get the category id (${id}): ` + e.message)
    }
  }
  //#endregion Categories_Queries

  //#region User_Queries
  const createUserIfNotExists = async props => {
    const {
      auth0_sub,
      nickname
    } = props;
    const answer = await db.get(
      SQL `SELECT user_id FROM users WHERE auth0_sub = ${auth0_sub}`
    );
    if (!answer) {
      await createUser(props)
      return { ...props,
        firstTime: true
      } // if the user didn't exist, make that clear somehow
    }
    return props;
  };

  /**
   * Creates a user
   * @param {Object} props an object containing the properties `auth0_sub` and `nickname`.  
   */
  const createUser = async props => {
    const {
      auth0_sub,
      nickname
    } = props;
    const result = await db.run(SQL `INSERT INTO users (auth0_sub, nickname) VALUES (${auth0_sub},${nickname});`);
    return result.stmt.lastID;
  }
  //#endregion User_Queries

  const controller = {
    // USER_CATEGORIES
    ins_UserCategory,
    del_UserCategory,
    upd_UserCategory,
    sel_UserCategory,
    sel_UserCategories,
    
    // USER ACCOUNT
    createUserIfNotExists,
    createUser,
  }

  return controller
}


export default initializeDatabase
