-- Balance
-- Automated
-- Result of All Incomes - Expenses
-- Should be updated everytime user refresh ? Yes maybe Should not be stored ?
CREATE TABLE user_balance (
	user_id text,
	current_balance integer DEFAULT 0 CHECK(current_balance >=0),
	weekly_total integer DEFAULT 0,
	next_weekly_total integer DEFAULT 0,
	monthly_total integer DEFAULT 0,
	next_monthly_total integer DEFAULT 0,
	yearly_total integer DEFAULT 0,
	next_yearly_total integer DeFAULT 0,
    FOREIGN KEY(user_id) REFERENCES users(auth0_sub)
);

-- Manual Creation (U.I)
-- Include Courant Transaction AND GOAL SAVING
-- Reverse algorithm

-- Total_Amount > Saving Data ?
-- Amount > +/- Amount
-- type_id {value, name}
	--> 0, Fixed Incomes
	--> 1, Recurring Icomes
	--> 2, Fixed Expenses
	--> 3, Recurring Expenses
	--> 4, Saving
-- Recurrence_type {value, name} (Based On Start/End Date)
	--> 0, Weekly
	--> 1, Monthly
	--> 2, Yearly

-- Start Date (Past | Present)
-- End Date (Future) > Also used for due date goal saving


CREATE TABLE user_transactions (
	guid integer PRIMARY KEY AUTOINCREMENT,
	title text,
	description text,
	total_amount integer DEFAULT 0,
	amount integer DEFAULT 0,
	start_date date,
	end_date date,
	type_id integer DEFAULT 0,
	isRecurring integer DEFAULT 0,
	recurrence_type integer DEFAULT 0,
	currency_type integer DEFAULT 0,
	category_id integer DEFAULT 0,
	user_id text,
    FOREIGN KEY(category_id) REFERENCES categories(id) ON DELETE CASCADE ON
    UPDATE CASCADE
    FOREIGN KEY(user_id) REFERENCES users(auth0_sub)
);

-- Manual Editing (U.I)
CREATE TABLE user_update_history (
	guid integer PRIMARY KEY AUTOINCREMENT,
	oldAmount integer DEFAULT 0,
	newAmount integer DEFAULT 0,
	date_mod datetime,
    FOREIGN KEY(guid) REFERENCES user_transactions(guid) ON DELETE CASCADE ON
    UPDATE CASCADE
)

-- Manual Creation (U.I)
CREATE TABLE user_categories (
	id integer PRIMARY KEY AUTOINCREMENT,
	name text,
	color text,
	user_id text,
    FOREIGN KEY(user_id) REFERENCES users(auth0_sub)
);
