import React, { Component } from "react";
import { withRouter, Route, Switch, Link } from "react-router-dom";
import Wallet from './Components/Pages/Wallet/Wallet'
import Profile from './Components/Pages/Profile/Profile'
import Category from './Components/Pages/Categories/Category'

// import "./App.css";
/* AUTH */
import * as auth0Client from "./Utils/Auth/Auth";
import SecuredRoute from './Utils/Routing/SecuredRoute';

/* NOTIFICATIONS */
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { pause, makeRequestUrl } from "./Utils/Utils.js";

import Sidebar from './Components/Sidebar/Sidebar';


// const makeUrl = (path, params) =>
//   makeRequestUrl(`http://localhost:8080/${path}`, params);

class App extends Component {
  render() {
    return (
      <div> 
        <Sidebar /> 
        <div Style= "margin-left:70px; padding: 0.01em 16px"> 
      <Switch>
        <Route path="/profile" exact render={()=><Profile />} />
        <Route path="/wallet" exact render={()=><Wallet />} />
        <Route path="/category" exact render={()=>  <Category />} />
        {/* <Route path="/callback" render={this.handleAuthentication} /> */}
        <Route render={() => <div>not found!</div>} />
      </Switch>
        </div>
        </div>
    );
  }
} 


export default withRouter(App);
