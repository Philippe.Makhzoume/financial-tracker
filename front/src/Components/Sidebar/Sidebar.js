import React, { Component } from "react";
import { NavLink } from 'react-router-dom'
import './Sidebar.css';

export default class Sidebar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isCollasped: false
        };
        this.toggleCollapse = this.toggleCollapse.bind(this);
    }

    toggleCollapse() {
        const collapse = !this.state.isCollasped;
        this.setState({ isCollasped: collapse });
    }

    render() {
        let toggleClassName = !this.state.isCollasped ? "main-menu" : "main-menu main-menu-expand";
        const Links = [
            {
                link:"/profile",
                icon:"fa fa-2x fa-angry",
                text:"Profile",
                position: ""
            },
            {
                link:"/wallet",
                icon:"fa fa-2x fa-cash-register",
                text:"Wallet",
                position: ""
            },
            {
                link:"/category",
                icon:"fa fa-2x fa-palette",
                text:"Category",
                position: ""
            },
            {
                link:"/#",
                icon:"fa fa-2x fa-power-off",
                text:"Logout",
                position: "logout"
            },
        ];
        return (
            <div>
                <div className="area">
                    <nav className={toggleClassName}>
                        <div>
                            <li>
                                <div className="main-menu-expand-color" onClick={this.toggleCollapse}>
                                    <i className="fa fa-bars fa-2x"></i>
                                    <span className="nav-text">
                                        Collapase menu
                                </span>
                                </div>
                            </li>
                        </div>
                        {Links.map((x, i) => (
                        <ul className={x.position} key={i}>
                            <li>
                                <NavLink to={x.link} activeStyle={{ color: "red" }}>
                                    <i className={x.icon} />
                                    <span className="nav-text">
                                        {x.text}
                                    </span>
                                </NavLink>
                            </li>
                        </ul>
                        ))}
                    </nav>
                </div>

            </div>
        )
    }
}